---
layout: post
title: "AC3"
date: 2014-09-03 09:45:19
category: communitycenter
---

​The Ambedkar Community Computing Center is not a private or public educational institution, it is a computing education center run by youths from a local group called Ambedkar Sanga that works in the slums of the area.

The Center was founded in 2007 in an effort to bring the world of computers close to underprivileged children and youths who otherwise would have little opportunity to get in contact with this technology.

The group consists of around 15 children and young boys and girls from 13 to 23 years old. They attend classes 3 times a week for 2 hours each session.

**Motivation**

The area in which this Center is located lacks even the most basic living conditions. We feel that the kids from this place should get a good thirst for Freedom while learning computing too, and Free Software serves this purpose. Free Software is a way to talk about Freedom.

**How We Did It**

No migration process was involved. We are Free Software advocates so we started directly by installing Free Software in all computers in use; therefore, we haven't faced any particular difficulties in setting up the systems. Our main difficulty regards the lack of hardware. We use only laptops, and we need as many as we can get. We rely on donations for our work.  
Commitment to Free Software

We have five work stations, all of which are laptops and all of them run fully free operating systems.

All of the programs we use are Free Libre software. For example, we useLibreOffice for spreadsheets and for general document writing, and GIMP for graphics.

**Results**

Browsing the web using Free Software helps these young boys and girls to have access to the external world in safety and confidence. Free Libre graphic programs such as GIMP allow them to express their creativity freely and easily.

One of the underprivileged kids who attends classes at the Center has achieved a high level of skill in the use of GIMP. His works have been presented and sold at a local Free Software conference. The picture on the right shows him handing in to Richard Stallman a copy of an essay titled “The Future is Ours”, which was produced at the Center.

Some of the kids who attend our classes also take computing courses at school. They report that the skills they learn at the Center are of invaluable help for them to follow and pass those courses successfully.

Courtsey gnu.org, https://www.gnu.org/education/edu-cases-india-ambedkar.html
