---
layout: post
title: "IEEE Tutorial on GNU/Linux Kernel"
date: 2013-02-10 02:16:21
category: events
---

Free Software Movement Karnataka(FSMK)<http://fsmk.org/> in coordination with IEEE Bangalore section, IBM Linux Technology Centre and BMSCE announces a Linux kernel tutorial.

IEEE announcement follows - IEEE Bangalore Section in association with BMS College of Engineering and Free Software Movement, Karnataka (FSMK) announces TWO DAY TUTORIAL ON GNU/LINUX Kernel Workshop By Eminent & Experienced Faculty from FSMK and IBM

**Date**     : May, 9th and 10th (Saturday, Sunday)  
**Venue**  : BMS College of Engineering, Bull Temple Road, Basavanagudi, Bangalore - 560 019

**Prerequisites:**  
Its expected that all participants will have:

1.  Basic knowledge of Operating Systems
2.  Good working knowledge of C
3.  Hands-on experience desirable
4.  Kernel programming experience is a bonus
5.  Basic scripting knowledge (ex: shell scripts)
6.  Working knowledge of  Gnu/ Linux. Ex:
7.  Booting a  Gnu/ Linux system
8.  Basic system administration (adding/removing users etc)
9.  Basic commands (ls, ps, chmod, mkdir, vi, etc)

We have planned to provide computer system for as many participants as possible, but would encourage, especially non-student participants ,to bring their own laptops so we can accommodate more numbers.

**Program:**

May 9th, (DAY 1)

8:30AM   onwards: Registration

9:30 -10:30 AM:  
                    Inauguration: (Venue: Auditorium, BMS College of Engineering)  
                    **Chair:**  
                            Mr. K.. Ramakrishna ,  
                            POWERGRID, Bangalore,  
                            Chairman, IEEE Bangalore Section  
                     **Chief Guest:**  
                            Dr. S. Bisalaiah, M.A , Ph.D,  
                            Former Vice- Chancellor, UAS, Bangalore,  
                            Member, Managing Body, BMSET,  
                            Chairman, Board of Management, BMSCE  
                     **Guest of Honour:**  
                             Dr. K Mallikharjuna Babu Ph.D.,  
                             Principal, BMS College of Engineering

10:30-11:00 AM:  Tea

11:00AM to 1:00 PM:  SESSION 1

**Basics of  Gnu/ Linux Programming & Tools**

1.  Simple C programs
2.  Simple Makefiles
3.  gcc, stace, ltrace, gdb, objdump, nm, LD_PRELOAD [EXERCISE]
4.  Quick overview of a typical hardware on which  Gnu/ Linux runs(CPU, Memory, Interrupt controller, IO Bus etc)
5.  Overview of the Linux Kernel
6.  Kernel Layout
7.  CodingStyle
8.  Important subsystems
9.  Kernel configuration
10. Building and booting a new kernel [EXERCISE]

1:00PM  to 2:00 PM:  LUNCH

2:00PM to 5:00PM  SESSION 2

**Linux Kernel Internals: An Overview (3 hours)**

1.  Process and threads [EXERCISE]
2.  Memory Management [EXERCISE]
3.  Filesystems [EXERCISE]
4.  Interrupts and exceptions & System Calls [EXERCISE]
5.  Kernel synchronization
6.  How is the operating system changing [DISCUSSION]
7.  What are the key areas of focus [DISCUSSION]
8.  Generic questions and answers

May 10th, (DAY 2)

9:30-10:30 AM SESSION 3

**Device Drivers (1 hour)**

1.  Kernel module programming
2.  Char and block drivers
3.  ioctls
4.  Writing simple char device driver [EXERCISE]

10:30:10:45 Tea

10:45-1:00PM SESSION 4  
**Kernel Debugging (2 hours , 15 Minutes)**

1.  printk
2.  Dynamic instrumentation techniques
3.  SystemTap and tracing (ftrace and markers) [EXERCISE]
4.  kexec/kdump/crash analysis [EXERCISE]

1:00 PM to 2:00 PM LUNCH

2:00PM to 5:00PM  
**How You can contribute**

1.  Introduction to the  free software community
2.  How to contribute
3.  New exciting areas of work in the kernel
4.  Generic questions and answers  Tutorial is organized in various sessions as above. Lunch and Tea are provided. Please report at 9am at the venue. Course Fee per participant: Non-member: Rs. 1500/-; Student  Rs. 700/- IEEE member Rs. 1200/- ; IEEE Student member Rs.550/-

**Organizing Committee:** Sethuraman Ganesan Chair,PAC, IEEE Bangalore Section Hitesh Mehta, Secretary, IEEE Bangalore Section Dr. T.Srinivas, SAC,IEEE Bangalore Section  Ravi Kiran A.,MDC, IEEE Bangalore Section V V Srinivasan,Treasurer, IEEE Bangalore Section

Please send your registration to: Mr. Sethuraman Ganesan  
                                                Chair - Professional Activities IEEE Bangalore Section  
                                                ABB Ltd,  
                                                Khanija Bhavan, 5th Floor, West Wing, 49, Race Course Road,  
                                                Bangalore 560001,  
                                                Phone: +91 99014 99033,  
                                                e-mail: sganesans@yahoo.com

Visit IEEE Bangalore Section at http://www.ieee.org/bangalore.
