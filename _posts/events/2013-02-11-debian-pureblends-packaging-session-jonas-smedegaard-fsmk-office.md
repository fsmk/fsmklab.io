---
layout: post
title: "Debian PureBlends Packaging session by Jonas Smedegaard in FSMK Office"
date: 2013-02-11 17:45:22
category: events
---

Dear All,

Debian Developer and FreedomBox Jonas Smedegaard has been in Bangalore and numerous events are being organized.  
http://wiki.jones.dk/DebianAsia2011#Bangalore

As a culmination to this stint of his stay in Bangalore, Jonas will be handling a Debian Pure Blends packaging session at FSMK Office, Bangalore.

**Agenda:**

1.  Understanding the differences between Debian Derivatives/Debian Blends/Debian Pureblends
2.  Debian Pure Blend in a hands on session
3.  Further interaction with Jonas on FreedomBox and other big plans in the Free Software community

Date: 6th November, 2011  
Time: 10 am  
Place: FSMK Office  
Free Software Movement Karnataka  
No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden,  
Bengaluru - 560030. Ph: 080-42817353

**Requirements:**

*   Preferably a Debian Machine!
*   Basic bash acquaintance

 
