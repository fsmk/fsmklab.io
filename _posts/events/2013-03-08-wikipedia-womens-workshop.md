---
layout: post
title: "Wikipedia Women's Workshop"
date: 2013-03-08 02:39:24
category: events
---

On the eve of International Women's Day, FSMK has organised a women's workshop in Bangalore. The idea is to get more women aware about Wikipedia and Wikimedia projects. They would be guided on "How to contribute to Wikipedia?" by an hands-on editing session

To participate in the event please fill the google doc link

**Event Details**  
This event is one of many around the world that aims to engage more women to edit Wikipedia, and to improve coverage about women and women centric topics on Wikipedia.

Date: Saturday, March 9th, 2013  
Time: 10am-4pm  
Venue: Jyoti Nivas College, Koramangala, Bangalore

**Intended Audience**

FSMK team members  
In-house students  
Faculty members  
students from other educational institutes.  
Its open to all and NO Registration Fee.

Wikipedia Women's Workshop
