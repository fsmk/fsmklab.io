---
layout: post
title: "Building interactive programs with Processing."
date: 2013-12-28 06:09:04
category: events
---

Building interactive programs, hands on, with Processing, a java-based language designed for beginners.

**Pre-requisites:**  
Download Processing from http://processing.org/download/?processing  
for the machine of your choice
