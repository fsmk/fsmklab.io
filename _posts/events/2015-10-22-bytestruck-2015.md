---
layout: post
title: "BYTESTRUCK 2015"
date: 2015-10-22 19:00:36
category: events
discourse_topic_id: 75
---

We are pleased to inform you that GLUG PACE is presenting Bytestruck '15 (for the 7th consecutive time).  
For more: <a href="http://bytestruck.in/" rel="nofollow" target="_blank">http://bytestruck.in</a>.
