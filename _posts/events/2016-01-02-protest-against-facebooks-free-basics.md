---
layout: post
title: "Protest against Facebook's Free Basics"
date: 2016-01-02 17:21:55
category: events
discourse_topic_id: 162
---

Today was a protest by ‪FSMK‬ activists. This is a part of the call by Free Software Movement of India to all its member organisations to ‪#‎SayNoToFreeBasics‬ and yes to ‪#‎NetNeutrality‬ From FSMK EC, Abhiram Ravikumar who is also a Mozilla rep, Ayesha Ishrath, Rameez Thonnakkal, Divya Thasma Shijil and Vikram Vincent addressed the gathering. Our unit PES College of Engineering GLUG (Gnu Linux User Group) also performed similar gathering in college premise in solidarity with main protest.

Pictures [here ][1]

 [1]: https://www.flickr.com/photos/124434814@N02/albums/72157662499311290
