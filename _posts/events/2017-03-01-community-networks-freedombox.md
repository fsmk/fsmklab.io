---
layout: post
title: Workshop on Low Cost Community Networks with FreedomBox
date: 2017-03-01 20:50:57 +0530
category: events
startdate: 2017-03-18 10:00:00 +0530
enddate: 2017-03-19 17:00:00 +0530
discourse_topic_id: 524
---

# About

Swecha and the FreedomBox project invite you to learn how to build
community Wi-Fi networks with FreedomBox. Community networks spanning
entire villages and campuses providing free Internet access and other
digital services such as free VOIP calls and radio services are
possible for a low cost of about INR 1,00,000 per location. The
workshop is meant for volunteers, hacktivists, enthusiasts and
entrepreneurs who are interested in understanding all the low level
details to replicate the success of Gangadevipally. This is a
training session to make the participants feel confident to lead an
effort to setup a community network.

Participants will walk away with the knowledge on technical and
organization aspects of setting up Wi-Fi networks. Documentation on
all of the discussed items will be available. Participants will also
get to engage with people involved or looking to get involved in
building networks.

# Details

Location: Hyderabad, India

Date: 18th and 19th of March, 2017

Time: 10:00 to 17:00

End of registration: 16th March, 2017

# Registration

You must register for the workshop by sending an email. The workshop
is expected to be small with mostly local participants and some from
neighboring states. Number of attendees we can accomodate is limited
and we will accept registrations on a first come, first serve basis.
Send us a registration email with the following details:

Subject should be "Registration for Workshop".
Body should contain
Names of the participants
Reason for interest
Organization name if any

# Facilities

There is no registration fee for the workshop. You are expected to
make your arrangements for travel, food and accommodation. Some
minimal arrangements may be available, please contact us for more
details.

# Contents

The following are the topics of discussion during the workshop.

### Understanding

Decentralization of networks
Why Wi-Fi?
Model of ownership

### Planning

Selecting a village/community to setup Wi-Fi
Approaching a community with proposal
Identifying suitability for target audience
Accumulating funds
Financial planning
Building a team
Training the team
Surveying for feasibility
Understanding users
Timetable for installation
Services to be run in the village
Planning for Internet connection
Planning a support/maintenance team

### Wi-Fi network

+ Mapping installation area with Open Street Maps
+ Designing a Wi-Fi network for large areas
+ Creating a bill of materials
+ Purchasing hardware
+ Purchasing tools for installation
+ Comparing Wi-Fi router hardware
+ Choosing the right antenna
+ Configuring OpenWrt Wi-Fi routers
+ Testing and mapping Wi-Fi signals
+ Power-over-Ethernet for Wi-Fi routers
+ Power boxes for Wi-Fi towers
+ Designing towers for Wi-Fi networks
+ Optimal deployment of Wi-Fi routers
+ Erecting Wi-Fi towers
+ Providing power to Wi-Fi towers
+ Outdoor deployment of Ethernet and power cables
+ 100 Mbps Ethernet Backhaul
+ Fibre Optic Backhaul
+ Understanding Wi-Fi roaming

### Long range point-to-point Wi-Fi links

+ Choosing hardware
+ Purchasing hardware
+ Configuring and testing the hardware
+ Choosing frequency/band
+ Link budget calculation
+ Installation of Wi-Fi hardware
+ Antenna alignment
+ Bandwidth measurement
+ Connecting to backhaul

### FreedomBox

+ Basic set up of FreedomBox
+ Setting up DHCP for entire network
+ Setting up Internet connection
+ Configuring SIP server (repro)
+ Configuring Ampache
+ Configuring NextCloud
+ Automatic security updates
+ Setting up bandwidth control
+ Setting up remote access into FreedomBox
+ Setting up monitoring tool

### Post installation

+ Registrations for users
+ Installing apps and configuring mobile phones for all services
+ Launch event
+ Monitoring
+ Building a village support team
+ Planning for computer availability in the village

# Contacts

Siddhartha Malempati +91-9490098016
Sunil Mohan Adapa +91-9848880699
