---
title: Fsmk Winter Camp '18
category: events
layout: post
pinned: false
date: 2018-01-10 00:00:00 +0000
startdate:  2018-01-22 
enddate: 2018-01-27
discourse_topic_id: 1082
---

![image](/images/All_web.png)


FSMK Camp 2018 is here!!!

FSMK Camps is an initiative by the Free Software Movement
Karnataka. Aimed at budding engineers and enthusiasts, we teach
participants to use Free Software, instead of proprietary software, to
develop their ideas into solutions. 

FSMK has been actively organizing workshops in various engineering
colleges across the state for seven years. However, we realized that a
residential camp, for a week or more, which is completely dedicated to
Free Software technologies is more useful to students as they are able
to interact more with the speakers and get more time to practice and
apply what they learn. So, in 2013, FSMK conducted two residential
camps for students which saw huge participation. In January 2013, we
conducted a five-day residential camp at Sai Vidya Institute of
Technology, Rajankunte, which was attended by about 160 students. In
July 2013, we conducted a nine-day residential camp at Jnana Vikas
Institute of Technology, Bidadi which was attended by about 180
students. In 2014, the camp was conducted at Dayanand Sagar College of
Engineering, and it was attended by 200 students. In 2015 we conducted
another camp in REVA Institute of Technology & Management which was
attended by 180 people. In 2016 we conducted one more camp in
Siddaganga Institute of Technology which was attended by 150
people. 

Our camps are organized by volunteers from the students, community
members, and industry professionals. FSMK Camp ‘18 is open to all
districts within the state. We have amazing activities planned with
multiple tracks to reach out to people with different interests.

For more details about the camp please visit
the
[discuss post](https://discuss.fsmk.org/t/winter-camp-2018/1082)


### Contact
Shijil : +91 8892324346

Voidspace : +91 99163 94958
