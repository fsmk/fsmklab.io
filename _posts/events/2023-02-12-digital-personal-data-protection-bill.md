---
title: "Let's Talk About - Digital Personal Data Protection Bill 2022"
category: events
layout: post
date: 2023-02-12 00:00:00 +0000
---

Free Software Movement Karnataka is organizing a discussion on the Digitial Personal Data Protection Bill which is under proposal. 
Every citizen of this country must understand what is being proposed under this bill and how will it affect us once it is passed. And what can we do about it. 

In 2017, the Supreme Court of India recognized the Right to Privacy as a fundamental right under the Constitution and laid down certain privacy principles relevant to informational privacy(i.e., data privacy). The court also acknowledged the absence of a comprehensive privacy law and noted the gaps present in the Information Technology (reasonable Security Practices and Procedures and Sensitve Personal Data or Information) Rules or SPDI Rules, notified in 2011, limiting its utility for protect. 

On November 18, 2022, the Ministry of Electronics and Information Technology proposed a new law, namely the Digital Personal Data Protection Bill 2022. Once passed by Parliament, it would replace the 2011 rules and some portions of existing law. Lets talk about the bill and undestand how would this going to impact us as citizen of India.  


Where: YWCA Hall, Near to Koramangala Police Station, 80 Feet Road, 20th Main, 6th Block, Koramangala
When : Feb12, 2023 4:00 PM

![Srinivas_Kodali_DPDPB2022](/images/dpdpb2022.jpeg)
