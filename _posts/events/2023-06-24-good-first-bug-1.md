---
title: "Good First Bug [24 June 2023]"
category: events
layout: post
date: 2023-06-17 10:00:00 +0530
---

Free Software Movement Karnataka is conducting Good First Bug 1 at CMR Institute of Technology, Whitefield, Bengaluru

This is a platform for anyone to get exposure to FLOSS contributions. The best way to start contributing to FLOSS is by attending beginner's bugs. Even though there is lots of content available on the Internet , many fail to accomplish fixing the first bug due various starting troubles. The 'Good First Bug' initiative to address such gaps.

For list of projects click [here](https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/blob/main/1/README.md)

**Register at** : [https://commune.fsmk.org/event/good-first-bug-1-4/register](https://commune.fsmk.org/event/good-first-bug-1-4/register)


**Where**: Architecture A.V. Hall, CMR Institute of Technology, CMR Institute of Technology, Whitefield, Bengaluru, Karnataka
**When** : June 24, 2023 10:00 AM

![Good_First_Bug_1_Poster](/images/GoodFirstBug_1.jpeg)
