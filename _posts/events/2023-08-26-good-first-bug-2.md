---
title: "Good First Bug [26 August 2023]"
category: events
layout: post
date: 2023-08-19 10:00:00 +0530
---

Free Software Movement Karnataka is conducting Good First Bug 2 at CMR Institute of Technology, Whitefield, Bengaluru

This is a platform for anyone to get exposure to FLOSS contributions. The best way to start contributing to FLOSS is by attending beginner's bugs. Even though there is lots of content available on the Internet , many fail to accomplish fixing the first bug due various starting troubles. The 'Good First Bug' initiative to address such gaps.

For list of projects click [here](https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/tree/main/2)

**Register at** : [https://commune.fsmk.org/event/good-first-bug-cmrit-12/register](https://commune.fsmk.org/event/good-first-bug-cmrit-12/register)


**Where**: Architecture A.V. Hall, CMR Institute of Technology, CMR Institute of Technology, Whitefield, Bengaluru, Karnataka

**When** : August 26, 2023 10:00 AM

![Good_First_Bug_2_Poster](/images/GoodFirstBug2-poster.jpg)
