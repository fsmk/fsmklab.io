---
title: "Good First Bug Christ Universtiy [30 August 2023]"
category: events
layout: post
date: 2023-08-23 10:00:00 +0530
---

Free Software Movement Karnataka is conducting Good First Bug at CHRIST (Deemed to be Universtiy), Kengeri Campus.

Good Firs Bug is a platform for anyone to get exposure to FLOSS contributions. The best way to start contributing to FLOSS is to by attending beginner's bugs. Even though there are lots of content available on Internet , many fails to accomplish fixing the first bug due various starting troubles. The 'Good First Bug' initiative to address such gap.

For list of projects click [here](https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/tree/main/2)

**Register at** : [https://commune.fsmk.org/event/good-first-bug-christ-university-13/register](https://commune.fsmk.org/event/good-first-bug-christ-university-13/register)

**Where**: CHRIST (Deemed to be University), Bengaluru Kengeri Campus, Kanmanike, Kumbalgodu, Mysore Road, Bangalore - 560074

**When** : August 30, 2023 10:00 AM




Please feel free to go through these materials before the event.

- [First Contribution Guide](https://github.com/firstcontributions/first-contributions)
- [MIT missing semester - Version Control](https://missing.csail.mit.edu/2020/version-control/)
- [Git & GitHub Crash Course For Beginners](https://www.youtube.com/watch?v=SWYqp7iY_Tc)
- [Git Cheat Sheet](https://cs.fyi/guide/git-cheatsheet)

- [Free software, free society: Richard Stallman at TEDxGeneva 2014​](https://www.youtube.com/watch?v=Ag1AKIl_2GM)
- [AT&T Archives: The UNIX Operating System](https://www.youtube.com/watch?v=tc4ROCJYbm0)


