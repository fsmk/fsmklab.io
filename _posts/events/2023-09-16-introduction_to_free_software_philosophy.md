---
title: "Introduction to Free Software Philosophy"
category: events
layout: post
date: 2023-09-10 10:30:00 +0530
---

**Let's celebrate Software Freedom Day**

[Software Freedom Day (SFD)](https://www.softwarefreedomday.org/) is an annual worldwide celebration of Free Software. SFD is a public education effort with the aim of increasing awareness of Free Software and its virtues, and encouraging its use.

**Event Details:**
**Speaker**: Prabodh C.P., Research Scholar at Indian Institute of Technology, Hyderabad
**Date**: 16 September 2023
**Time**: 10:30 A.M. to 11:30 A.M.
**Platform**: Online

**Register at** :  [https://commune.fsmk.org/event/introduction-to-free-software-philosophy-18/register](https://commune.fsmk.org/event/introduction-to-free-software-philosophy-18/register)

The meeting link will be shared to registered participants via email

![SFG_Banner](/images/ilovesfd.png)
