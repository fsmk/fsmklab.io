---
title: "Faculty Development Program (FDP) - Data Analytics and Visualization using Python based Free and Open Source Software"
category: events
layout: post
date: 2023-11-01 10:30:00 +0530
---


We are conducting 2 day Faculty Development Program (FDP) on Data Analytics and Visualization using Python based Free and Open Source Software. This would be the first FDP based on our lab manual project.

# Data Analytics and Visualization using Python based Free and Open Source Software

## Day 1

### Inauguration

---

**Session 1** : 09:30 AM - 10:00 AM  
**Topic**: Introduction to FOSS and its significance in Curriculum

---

**Session 2** : 10:00 AM - 11:00 AM  
**Topic**: Introduction to FOSS and its significance in Curriculum

---

**Session 3** : 11:15 AM - 01:00 PM  
**Topic**: Setting up the Anaconda Environment & usage of the IDE

---

**Session 4** : 02:00 PM - 04:30 PM  
**Topic**: Numpy & Pandas for Data Analysis

---

## Day 2

### **Session 1** : 09:30 AM - 11:00 AM  
**Topic**: Matplotlib and various plot types

---

**Session 2** : 11:15 AM - 01:00 PM  
**Topic**: Seaborn, Bokeh, Plotly and few use cases

---

**Session 3** : 02:00 PM - 04:30 PM  
**Topic**: Data Analysis with LibreOffice

---

### Valedictory  
**Session 4** : 04:30 PM  


![FDP_AMC_Page_1](/images/AMC_FDP_Page-1.png)
![FDP_AMC_Page_2](/images/AMC_FDP_Page-2.png)
![FDP_AMC_Page_3](/images/AMC_FDP_Page-3.png)
