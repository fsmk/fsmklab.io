---
title: "Open Game Party"
category: events
layout: post
date: 2024-08-10 10:00:00 +0530
---

Hello Gamers !


FSMK is hosting a gaming tournament focusing on FOSS games.
We will have a small general talk about the gaming industry and its increasing impact followed by a tournament with three games.

Participants will play three multiplayer games of different genres in succession :
- Xonotic ([https://xonotic.org/](https://xonotic.org/))
- Hedgewars ([https://hedgewars.org/](https://hedgewars.org/))
- mk.js ([https://mk.mgechev.com/](https://mk.mgechev.com/))

Players who collect the greatest number of points cumulatively across games win the event !

Winner Prizes include t-shirts and stickers ! Entry is free.

Date : **August 17th**
Time : **2:00 PM**
Venue : **FSMK Office** - [https://maps.app.goo.gl/CN8PxKuhZgTUtJLLA](https://maps.app.goo.gl/CN8PxKuhZgTUtJLLA)

**Register** at [https://commune.fsmk.org/event/open-game-party-25/register](https://commune.fsmk.org/event/open-game-party-25/register)

**Note : Participants are to bring their laptop and any peripherals they require [ mouse, keyboard etc]**

![Open_Game_Party](/images/Open_Game_Party.png)