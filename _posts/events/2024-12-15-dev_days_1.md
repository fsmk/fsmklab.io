---
title: "Dev Days - Building Modern Apps using GenAI with FlowiseAI"
category: events
layout: post
date: 2024-12-08 10:00:00 +0530
---

**Join us for Dev Days**


In its efforts to enable communities, **FSMK** is launching a technology upskilling platform **#FsmkDevdays** for Free and Open source enthusiasts to explore and share their experiences in next-gen technologies. 

As part of this, the first **#FSMKDevdays** will be held at its office, In this sessions **Ranjit Raj** a well known face is India FOSS community will take a hands on session on “**Building Modern Apps using GenAI with FlowiseAI**"

Prerequisite:

- **A laptop is required for this workshop.**


**Register** at : [https://commune.fsmk.org/event/dev-days-building-modern-apps-using-genai-with-flowiseai-29/register](https://commune.fsmk.org/event/dev-days-building-modern-apps-using-genai-with-flowiseai-29/register)

![Dev_Days_1](/images/dev_days_1.png)