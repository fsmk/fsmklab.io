---
layout: post
title: "GLUE"
date: 2015-06-05 10:34:48
category: glugs
---

GECH Linux Users and Enthusiasts abbreviated as GLUE is a group of free software enthusiasts from Government Engineering College Hassan , who believe in learning, exploring  and sharing free software technologies . For the same GLUE conducts regular meetups and workshops.  
Being established in 24/08/2013 GLUE is heading towards its 2nd anniversary. We conduct regular meetups during Saturdays and spread the knowledge among the GLUE members. Members are increasing gradually and is now having 15 core members and 25 active members in the GLUE. Many of us contributed for  few open source projects like wikipedia, localization, scrollback and frequently reporting the bugs that are encountered while using the open source softwares in our day to day life.  

Few links of GLUE:  
<a href="https://gluegechassan.wordpress.com/" target="_blank">https://gluegechassan.wordpress.com/</a>  
<a href="https://colourfulllrainbow.wordpress.com/2014/08/18/glue-session-16082014/" target="_blank">https://colourfulllrainbow.wordpress.com/2014/08/18/glue-session-16082014/</a>  
<a href="https://colourfulllrainbow.wordpress.com/2014/09/08/python-workshop/" target="_blank">https://colourfulllrainbow.wordpress.com/2014/09/08/python-workshop/</a>  
<a href="https://colourfulllrainbow.wordpress.com/2014/10/06/glue-celebrated-sfd/" target="_blank">https://colourfulllrainbow.wordpress.com/2014/10/06/glue-celebrated-sfd/</a>  

<a href="http://www.scrollback.io/glue" target="_blank">http://www.scrollback.io/glue</a>  

<a href="https://www.facebook.com/groups/gluegechassan/" target="_blank">https://www.facebook.com/groups/gluegechassan/</a>
