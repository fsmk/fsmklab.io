---
title: Why Localization
date: 2017-04-12 08:02:00 +0530
layout: post
category: opinions
discourse_topic_id: 731
---

This originally originated as an exercise during the monthly meeting on March 12th, 2017. @ssrameez held a blogging workshop where the people present there were divided into groups and were asked to write a blog post wither using any of the topics provided or using any topic of their own choice.

Our Group (Shijil, Anagha, Pranathi, Shreya, and Sohom) decided to write about Localisation and this is what came out of it.

(this post is long overdue (almost a month overdue))

---

Localization at a basic level means to make a software available for the locals in a community using a local language rather than the language in which the software was originally packaged. This language can be any language that the locals of that community are comfortable in.

Today most of the softwares that are out there are created at an international level using international tools. Internaltional tools are the programming languages and libraries and web services and what not that are used by developers at an international level. English is the most common mode of communication (at least in the computer programmer context) and thus this model of development makes sense for the develeoper.

However we need to think about the user also because without the user a piece of software is nothing. It is the user who makes a software successful or a failure.
After all why do we write software. We write software primarily for two main reasons. Firstly we write software so that it makes our lives easier and Secondly we write software so that people who do not know how to write software can use them to make their life easier also..

But there is a catch! A majority of the people living on the planet do not speak english and/or english is not their first language. Does this mean that they do not have equal rights to information? Does this mean that they should be denied access to information? Is it morally right that these people need to learn another language just so that they can use a piece of software?

NO.

This is not right and most of the time what happens is that these people choose not use a software instead of learning a new language (I mean let's just honesnt with ourselves; would you learn a foreigh langugage just so that you could use some fancy software ??) and the world loses a great deal of human potential, human potential that would otherwise be usefull and have a positive effect on the society.

THIS IS WHY LOCALIZATION IS IMPORTANT!

Contrary to popular belief localization is not about the software instead it is about the people who use or who are the potential users of the software.

This is also an aspect of Free and Open Source Software that is truely about the people and not about the technology. And the barrier to entry is very very less for localization. One does not need to be a computer programmer to start localising a software. All they need is the willingness to localize a software, some amount of love for the language and some amount of love for the people. There are tools out there that make the process very easy for beginners to get started.

So... Good luck ...(with localization)

---

Comments, modifications, edits are welcome.
