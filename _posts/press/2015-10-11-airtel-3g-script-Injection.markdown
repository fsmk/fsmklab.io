---
layout: post
title:  "Airtel 3G Script Injection"
date:   2015-10-11 19:50:01 +0530
category: press
---





We as a community are shocked to see the response, an Israeli company, Flash Networks, purporting to be the creators of the software developed for Airtel's use also, has issued a 'Cease and Desist' notice, that threatens civil and criminal action against the Bangalore based developer Thejesh GN.

FSMI appeals to all other groups working on net neutrality to oppose Airtel and Flash Networks illegal actions and share this code widely, thus defeating corporate attempts to muzzle citizens right to free speech.

The statement is as follows: 

On June 3, 2015, a Bangalore based programmer and activist Thejesh GN, exposed certain details of a software program utilised by the Airtel 3G network, which injected malicious code into a user's web browser, allegedly without notifying or taking the consent of the user. This code collected users' data and inserted advertisements into the data stream being received by all Airtel customers on its 3G networks.

This is a clear violation by Airtel of:

    the principles of carriage (of telecommunications) and the terms of the licenses issued by the Government of India to service providers. This requires a service provider not to interfere with the communications / data stream of their consumers;
    the principle of network neutrality – in that Airtel was able to prioritise its advertisements to users by utilising the fact that it was the source of access to the Internet;
    as the program collects users data, this violates the users right to privacy - protected by the Constitution of India and various statutes as well as the service provider's license. Thejesh uploaded screenshots and some text explaining the relevant portion of the code (highlighting the malicious script injection) on the global code depository - github.

In response, an Israeli company, Flash Networks, purporting to be the creators of the relevant software, have issued a 'Cease and Desist' notice, that threatens civil and criminal action against him, for taking the dastardly step of exposing a malicious function in a program that acts so as to illegally invade the privacy of all customers of Airtel's 3G networks. The company has also issued a take down notice to Github under the US law – the Digital Millennium Copyright Act, requiring them to disable access to the information posted online in public interest. Github has now taken down the code. This is nothing but an attempt at bullying an Indian citizen for publicly disclosing Airtels' and its Israeli partner, Flash Networks' violations of Indian citizens rights of online access and privacy and to cover up their methods of using unethical, immoral and illegal means to make profits. The initial violations of privacy and network neutrality are now being compounded by an attempt to violate an Indian citizen's rights to free speech, in order to avoid exposure of corporate malfeasance.

Exposure of the malicious software being utilised by Airtel is not only in public interest, it is explicitly protected under the Indian Constitutional and relevant statutes. First, there is no contract between Flash Networks and the user of Airtel's network – and therefore a license covering the software cannot, per se, be enforced by Flash Networks against any individual user. Second, the Indian Copyright Act, 1957, clearly protects reproduction of portions of a computer program in certain circumstances that constitute fair use. Specifically, “the observation, study or test of functioning of the computer programme” is not a violation of copyright. Companies cannot surreptitiously insert code on users machines and then argue that such code should not be made public. The very fact that this code is surreptitiously inserted in the browsers of all Airtel's 3G network customers gives them the right to review such code and discuss this code in public – in addition to the right to take appropriate legal recourse against Airtel. The fact that the program was not reproduced in whole or indeed that there was no commercial motive whatsoever behind making the source code public, only makes the applicability of the fair use exceptions all the more clear. It is also interesting to note that despite Airtel having disowned all knowledge of the situation, it has not disclosed how is it that its 3G services are introducing this code, purportedly belonging to Flash Networks, into users browsers.

Accordingly, the FSMI demands that:

    The relevant authorities – the Government of India, Department of Telecommunications and / or the Telecom Regulatory Authority of India - - should immediately initiate an inquiry and take action against Airtel for interfering with the communications of its users on its 3G networks, allegedly without consent or notification to this effect;
    All telcos should be directed to ensure net neutrality, unimpeded access to the Internet and protect privacy of its users; and submit reports on what measures they have taken to give effect to the same;
    Given the increasing number of violations of privacy and net neutrality seen in the online sphere, the Government and other relevant authorities act with appropriate urgency to put in place specific public interest regulations pertaining to privacy of online communications and net neutrality.
