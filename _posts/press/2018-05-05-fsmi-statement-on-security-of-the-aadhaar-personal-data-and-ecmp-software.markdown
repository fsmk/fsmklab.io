---
title: FSMI statement on Security of the Aadhaar personal data and ECMP Software
category: press
layout: post
pinned: true
date: 2018-05-05 00:00:00 +0000
---

FSMI <a href="https://cloud.fsmk.org/index.php/s/r8ppLJR2wBKmOFZ" target="_blank">writes</a> to the the CEO of UIDAI about a patched version of the Enrollment Client Management Platform (ECMP) software used for off-line Aadhaar enrollment, which can potentially be used to bypass geo-location and bio-metrics, and also change the mapping between personal data of Aadhaar holders and their bio-metric data. Given the seriousness of this issue and the imminent threat to our national security given the widespread use of Aadhaar for identification purposes, we hope that UIDAI would treat this matter with utmost seriousness.
