---
title: "Press Note: Condemnation of Internet Shutdown in Karnataka and other parts of India"
category: press
layout: post
date: 2019-12-20 00:00:00 +0000
---

The ongoing nation-wide resistance against the Citizenship Amendment Act and the proposed National Register of Citizens exercise is growing everyday. The continuing repression of people participating in the protests by the autocratic government is a public knowledge now. Sadly, this repression has led to the death of two people in Mangaluru.

Moreover, to prevent protestors from voicing their dissent, the government has suspended internet services in Mangaluru and Dakshina Kannada regions. This shutdown is a continuation of such repressive actions in other parts of the country such as J&K, North Eastern States, UP, and Delhi.  FSMK strongly condemns the government for undertaking such draconian measures to suppress the protests of the common people.

In a landmark judgement in September, 2019, Kerala High Court bench ruled that Internet is a fundamental right of our citizens. Hence, the recent decisions by the government to arbitrarily shutdown internet and telecommunication services in several regions of the country must be seen as a direct infringement on people's fundamental right.

FSMK strongly believes in united action by people for positive changes in our society.  An alternate to Internet based services to organize people for mass action can be found using the Android applications below.

Using this application, people can contact each other and exchange information without Internet.

______

Important Notice: People are requested to take caution and make an effort to validate the authenticity of any information they receive on Internet or Mesh Net.

Links:-

[1]
Manyverse (A social network off the grid) https://www.manyver.se/

On F-Droid : https://f-droid.org/app/se.manyver

On Play Store: https://play.google.com/store/apps/details?id=se.manyver

[2]
Briar (Secure Messaging, Anywhere) https://briarproject.org/

On F-Droid : https://f-droid.org/app/org.briarproject.briar.android

On Play Store: https://play.google.com/store/apps/details?id=org.briarproject.briar.android

[3]
Rumble (Off-the-grid micro-blogging application for communities)

On F-Droid : https://f-droid.org/app/org.disrupted.rumble

----

Naveen Mudunuru
General Secretary
FSMK
