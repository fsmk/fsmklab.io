---
layout: post
title:  "Condemn the arrest of Disha Ravi"
date:   2021-02-15 17:43:01 +0530
category: press
---

Free Software Movement Karnataka Strongly condemns the arrest of Disha Ravi, a climate change activist for supporting the Farmers Struggle and linking it to the Online ToolKit released by Greta Thunberg.  We understand that she has been an activist bringing voices to communities in need. While authorities are trying to set an example through Disha Ravi, this arrest will only amplify the ideals that she has been standing for. This arrest is yet another blow to the basic democratic rights and freedom of speech of the people of this country. Delhi Police, which is under the control of the Central Government is making a mockery out of the constitution of this country.  It is stooping too low while waging this war against the critical thinking citizens of this country. The state is demanding followers and not wanting activists who mobilize the masses against its anti-people policies.

We urge the activists and friends of the Free Software Movement Karnataka to protest against this injustice. It is time for all of us to join hands to protect our right to speech and right to protest.

(Sign)
Secratary
Free Software Movement Karnataka


