---
layout: post
title: "FSMK Newsletter Volume 06"
date: 2013-04-08 18:27:04
category: updates
---


FSMK is releasing another edition of its Newsletter.

We have already discussed lot of policies and politics of free-software. It has enhanced the importance of FSMK day by day. Today we are interacting with many people like you. It is ultimately propagating the necessity of free-software and the message of freedom!

Come and join the hands of FSMK and be part of a freedom movement.

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK_Issue6_Nov_2009.pdf" style="margin: 0px; padding: 0px; text-decoration: initial; color: rgb(38, 140, 108); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 06</a>
