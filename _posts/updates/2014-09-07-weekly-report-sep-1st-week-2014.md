---
layout: post
title: "Weekly Report Sep 1st Week 2014"
date: 2014-09-07 18:13:12
category: updates
---

What Happened

#GLUGDSCE  
Free software philosophy session by Sarath on 4th Sep  
Python workshop  from 6th Sep to 7th Sep by PyCon team.

#TFSC-BMSIT  
Python workshop  from 6th Sep to 30th Aug by PyCon team.

#RITGLUG  
Free software philosophy session by Rakesh and RaghuRam on 9th Sep

#MCEGLUG  
Free software philosophy session by Rakesh 9th Sep

#GLUE  
Python workshop  On 6th Sep PyCon team, coordinated by Samruda

#FSMKSIT  
Free software philosophy session by Prabodh 5th

#TOGGLE  
Python workshop  On 4th Sep PyCon team, coordinated by Shijil and Neeta

#SJBITGLUG  
Problem solving Techniques session by GLUG Team

#PLUGIN  
Installation Fest on 5th September

Upcoming Activities - Reported so far

#CMRITGLUG  
Python workshop on 11th Sep
