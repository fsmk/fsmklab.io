---
layout: post
title: "WeTheGoondas"
date: 2015-03-07 16:27:39
category: updates
---

In the recently concluded session of Karnataka Assembly, the government tabled and passed a new amendment to the Goonda Act, which allows the government to 'preventively detain' people who it may consider a threat to public order. Amongst a largely mixed bag of disconnected offenses, it has also included IT and copyright infringements as 'goonda-ism', for which even without a crime, the police an arrest someone and keep them in custody 3 months or even upto an year without trial, merely on the suspicion of a future wrong doing. Although there are a few safeguards, it is nevertheless a draconian law that puts a lot of us in danger and has the potential to throttle free speech. To raise awareness we have launched the campaign 'We The Goondas'. Being part of the digital world know that you can also be jailed one day under Goonda act for unknowingly sharing a copyright protected image or PDF. Technically we all are Goondas in front of Karnataka government the day from the new law is in effect. Join us in the campaign in what ever manner you can..
