---
layout: post
title: "RTE Campaign"
date: 2015-03-12 20:12:50
category: updates
---

RTE act got materialized in the year of 2010 which describes the modalities of the importance of free and compulsory education for children between 6 and 14 in India. India became one of 135 countries to make education a fundamental right of every child after the law came in effect.

The RTE Act provides for the Right of children to free and compulsory education till completion of elementary education in a neighborhood school. It clarifies that ‘compulsory education’ means obligation of the appropriate government to provide free elementary education and ensure compulsory admission, attendance and completion of elementary education to every child in the six to fourteen age group.

Respective state Education department has to ensure RTE admission done on time before private school starts their own admission. In Karnataka it is DPI takes care of all RTE related processes. This term all RTE process made online cutting down all extra time and effort that parents may have to invest for visiting BEO office , Tahasiildar office etc to get her child admitted through RTE.

Since the entire RTE process made online , the situation of many would be the same as many does not have  access to computer and internet, hence we decided involve.

Free Software Movement Karnataka a not for profit organization stands for promoting free and open source technologies and open knowledge decided to take up RTE admission process as a helping hand towards parents from underprivileged session. FSMK mainly campaigned in areas where ever we run community center, an initiative to train under privileged kids in basic computer education to contribute our best to eradicate growing digital divide.

It was an experiment for us to take up such a campaign which involved lot of effort. ITEC a welfare forum for IT employee , helped us to collect information about RTE, we attended couple of meeting with expertise in this domain for the better understanding.

On first week of Jan this year we met at Jaynagar community center with ITEC , BGVS representatives and our core community center members, it was an open discussion . We had prepared a questionnaire in advance which we presented in front of others during the discussion. This discussion helped us to understand technical aspect of RTE process which was very much necessary before we go to ground.

Later on we decided to start campaign in AC3 area , we ten people gathered together and visited around 40 households meeting kids and parents, also distributed pamphlets.

The reason why we started campaign quite early was to ensure that kids will not be denied entry through RTE due to missing certificates. Similar way we campaigned in Jayanagar 9th block area visiting around 60 households. Later we followed up with their parents 2-3 times. We used to have conf call every day to keep us updated on campaign progress during that time. Myself , Jeeva , Mani ,Veeramma and Renuka used to be on call.

On 19th Feb RTE portal made available to public , bugs in portal and discrepancies in data made us stuck  proceeding with applying, we were panicked first couple of days. We contacted BEO officer in BTM asking no of queries , luckily he was patient enough to answer our queries. In turn Mani and Karthik visited him once.  Later they fixed issues.

By mid of Jan we started getting enquirers and parents started submitting forms , we thought we can do one more round of campaign whichever area it is possible. Hence we decided to print more pamphlets ,banners and posters both in Kannada and English . Hariprasad came forward to take up responsibility of campaigning in his area RajajiNagar where he involved local people for his help. Campaign was effective, Jeeva,Mani and myself started receiving call from different area, Jeeva received max call an average of 3-5 call every day.

By that time we had collected around 50 forms and we decided to meet at FSMK office for filling RTE forms with 6-8 community center members. It took 30 min for us to fill first application form , we were trying to reach RTE helpline which used to be busy always. But slowly we were coming on track in another 2-3 hours we filled all 30 forms we collected till then.ivya, Kavitha Jeeva and Hari were forefront filling forms, Mani , Jeeva and Me were scanning photos of students and adjusting resolutions. Dharmendra and Manjunath were conveying parents about filing application.Karthik was helping us too.

<div>
  <p>
    Later on we started filling forms as and when we received it , Jeeva himself might have filled around 80 forms .Meanwhile we got a request to visit to one slum near TilakNagar where parents wanted our help to fill RTE application. Jeeva along with Vignesh visited , there we filled around 15 appliation.
  </p>

  <p>
    By the end of application submission date approximately we could fill 150+ RTE application forms , though it is very minimal no we all are glad that we could do at least this…
  </p>

  <p>
    -Shijil
  </p>
</div>
