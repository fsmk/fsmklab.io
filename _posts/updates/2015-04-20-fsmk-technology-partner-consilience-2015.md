---
layout: post
title: "FSMK is the Technology Partner for Consilience 2015"
date: 2015-04-20 19:23:12
category: updates
---

FSMK is the Technology Partner for Consilience 2015, an annual flagship Conference hosted by the Law and Technology Society of NLSIU.The theme for this year's Consilience is Net Neutrality. Eminent speakers representing both sides are going to present their views including Nikhil Pahwa from Medianama, Kiran Jonalgaddha from HasGeek, Pranesh Prakash from CIS-India, T V Ramachandran from Vodafone Essar Ltd and Rajan Matthews from COAI. More info at http://consilience-nls.com/
