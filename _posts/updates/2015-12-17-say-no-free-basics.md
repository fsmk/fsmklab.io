---
layout: post
title: "Say no to free basics"
date: 2015-12-17 18:21:51
category: updates
---

On December 17, **Facebook** unleashed a massive campaign trying to win support for its Free Basics platform, which has been the subject of a lot of controversy.This is in the context of the consultation paper put out by the Telecom Regulatory Authority of India, which looks into the issue of differential pricing.

The paper stresses the need for ensuring non-discrimination, competitiveness and transparency in data tariffs and asks for recommendations for the same. Rightly fearing that its attempts to monopolize the internet and create a walled garden will be stopped, Facebook is now trying to galvanize the support of its users, many of whom unwittingly signed up in support of Free Basics. This is classic **Facebook**-style **manipulation** to garner support of Indians.

Over the past few months, Facebook has received a huge amount of criticism for its attempts to wall the internet through Free Basics, which they had originally called internet.org. Even Mark Zuckerberg's many publicity attempts have not succeeded in resolving the concerns of the Indian technology community and people. This consultation paper by TRAI is a good opportunity to take a firm stand against content providers acting as gatekeepers. It is now essential that we all work together to foil this sort of manipulation by Facebook.

FSMK calls upon all programmers, hacktivists, Facebook users and concerned citizens to rise to the occasion and work towards having a free internet and stop the balkanisation of internet.
