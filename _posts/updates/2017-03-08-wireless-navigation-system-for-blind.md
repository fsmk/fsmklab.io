---
title: Wireless Navigation System for Blind
category: updates
layout: post
date: 2017-03-08 19:16:00 +0530
discourse_topic_id: 542
---

As a part of Freedom Hardware Movement at FSMK, we are working on a project **Wireless navigation System for Blind**. An attempt to create a economically feasible and flexible navigation system for blind using Open Hardware and Software tools. It was chosen as our final year project to implement it in real-time use.
On 2nd March, we gathered at FSMK Office to work on the same and completed part of it and showcased it on Open Day 2017. We Interfaced ultrasonic sensor with Arduino and configured it as obstacle detector for particular range.
Again we will be gathering on 9th March @ FSMK Office to work on the project and to learn basics of Arduino and KiCad. Some college students,whom we meet at Open Day, will be joining us to discuss about Freedom Hardware. Interested can join us at around 11:30 AM at FSMK Office. Lets play with electronics.
