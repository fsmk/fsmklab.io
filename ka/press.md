---
layout: dynamic_page
title: ಪತ್ರಿಕಾ ಹೇಳಿಕೆ
permalink: /ka/press/
language: ka
---

<div class="row row-margin">
  {% for post in site.posts %}
  {% if post.language == 'ka' and post.category == 'press' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
